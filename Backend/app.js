const express = require("express");
const mongoose = require("mongoose");
// const compression = require("compression");
const cors = require("cors");
const bodyParser = require("body-parser");
const ip = require('ip')
const ordersRouter = require("./routes/orders");
const bot = require("./bot");
require("dotenv").config();

const PORT = process.env.PORT || 3001;
const app = express();
const front = __dirname + "/dist";

mongoose
	.connect(process.env.DB_URL)
	.then(() => {
		console.log("DB Connected");
	})
	.catch((err) => {
		console.log("DB Connection ERROR:");
		console.log(err);
	});

// app.use(compression());
app.use(express.static(front));
app.use(cors());
app.use(bodyParser.json());
app.use("/api", ordersRouter);

app.get("/", (req, res) => {
	res.sendFile(front + "/index.html");
});

console.log(ip.address());

app.listen(PORT, ip.address(), () => {
	console.log("Server started:");
	console.log("http://" + ip.address() + ":" + PORT);
});
